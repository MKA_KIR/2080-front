import Custom_link from "../shared/CustomLink/Custom_link";

import styles from "./main_grid.module.scss";

const MainHome = () => {
    return (
        <div className={styles.main_grid}>
            <Custom_link href="/read" text="Читати" className={styles.main_grid_text_read} />
            <Custom_link href="/buy" text="Купити" className={styles.main_grid_text_buy}/>
        </div>
    );
};

export default MainHome;
