
import styles from "./productCard.module.scss";

import Image from "next/image";

const ProductCard = ({name, image, price}) => {
    return (
        <div className={styles.card}>
            <div className={styles.img}>
                <Image src={image} alt={name} layout="fill" />
            </div>
            <h4 className={styles.name}>{name}</h4>
            <p className={styles.price}>{price}</p>
        </div>
    );
};

export default ProductCard;
