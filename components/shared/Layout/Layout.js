import Navbar from "../Navbar";

import styles from "./layout.module.scss"

export default function Layout({children}) {
    return (
        <>
            <Navbar/>
            <div className={styles.content}>
                {children}
            </div>
        </>)
}
