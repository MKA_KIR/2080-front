import Custom_link from "../CustomLink";
import Ticker from "../../Ticker";

import styles from "./navbar.module.scss";

const Navbar = () => {
    return (
        <div className={styles.wrapper}>
            <div className={styles.navbar}>
                <div>
                    <button className={styles.btn_language}>en</button>
                </div>
                <Custom_link href={"/"} text="2085" className={styles.logo} />
                <div>
                    <Custom_link href={"/contacts"} text="Контакти" className={styles.navbar_link}/>
                    <Custom_link href={"/buy"} text="Купити" className={styles.navbar_link}/>
                    <Custom_link href={"/basket"} text="0" className={styles.navbar_link}/>
                </div>
            </div>
            <Ticker/>
        </div>
    );
};

export default Navbar;
