import Custom_link from "../CustomLink/Custom_link";
import navbar_styles from "../Navbar/navbar.module.scss";

const TickerLink = () => {
    return (
        <>
            <Custom_link href={"https://www.instagram.com/2085brewery/"} text="instagram" className={navbar_styles.navbar_link} />
            <Custom_link href={"https://telegram.me/brewery2085_bot"} text="Telegram" className={navbar_styles.navbar_link}/>
            <Custom_link href={"https://2085beer.com/tel:+380684348341"} text="tel:+380 68 434 83 41" className={navbar_styles.navbar_link}/>
        </>
    );
};

export default TickerLink;
