import Link from "next/link";

export default function Custom_link({text, href, className}){
    return(
        <Link href={href}>
            <a className={className}>{text}</a>
        </Link>
    )
}
