import ProductCard from "../ProductCard/ProductCard";

import styles from "./product_list.module.scss";

const ProductList = ({list}) => {
    const productList = list.map(({description, composition, ...rest}) => rest);
    const productElements = productList.map(({id, ...rest}) => <ProductCard key={id} {...rest} />)
    return (
        <div className={styles.container}>
            {productElements}
        </div>
    );
};

export default ProductList;
