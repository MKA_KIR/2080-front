import Layout from "../components/shared/Layout";
import Buy_Categories from "../components/BuyCategories";
import Product_list from "../components/ProductList";

import {productList} from "../data/productList";

const Buy = () => {
    return (
            <Layout>
                <Buy_Categories/>
                <Product_list list={productList}/>
            </Layout>
    );
};

export default Buy;
