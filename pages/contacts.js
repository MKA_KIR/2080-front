import Custom_link from "../components/shared/CustomLink";
import Layout from "../components/shared/Layout";

import styles from "../styles/pages/contacts.module.scss"

const Contacts = () => {
    return (
        <Layout>
            <div className={styles.contacts}>
                <div className={styles.contacts_left_col}>
                    <iframe
                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2540.996938852913!2d30.518128815731096!3d50.44115767947402!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40d4ceff3e20a46b%3A0x10089f50b1d32668!2z0YPQuy4g0JHQvtC70YzRiNCw0Y8g0JLQsNGB0LjQu9GM0LrQvtCy0YHQutCw0Y8sIDUsINCa0LjQtdCyLCAwMTAwMQ!5e0!3m2!1sru!2sua!4v1626947349697!5m2!1sru!2sua"
                        width="100%" height="100%" style={{border: 0}} allowFullScreen="" loading="lazy"></iframe>
                </div>
                <div className={styles.contacts_right_col}>
                    <div>
                        <h3 className={styles.contacts_heading}>загальна інформація</h3>
                        <p className={styles.contacts_info_address}>ТОВ узвар, КИЇВ, вул. Велика Васильківська, буд.
                            5</p>
                        <h3 className={styles.contacts_heading}>відділ продажу:</h3>
                        <p className={styles.contacts_info}>
                            <Custom_link href={"mailto:"} text="2085brewery@gmail.com"
                                         className={styles.contacts_link}/>
                        </p>
                        <p className={styles.contacts_info}>
                            <Custom_link href={"tel:+380684348341"} text="+380 (68) 434 83 41"
                                         className={styles.contacts_link}/>
                        </p>
                    </div>
                </div>
            </div>
        </Layout>
    );
};

export default Contacts;
