import Buy_link from "../components/BuyCategories/Buy_Categories";
import Layout from "../components/shared/Layout";
import Merch_list from "../components/MerchList/Merch_list";

const Merch = () => {
    return (
            <Layout>
                <Buy_link/>
                <Merch_list/>
            </Layout>
    );
};

export default Merch;
