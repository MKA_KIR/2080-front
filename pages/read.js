import Image from 'next/image';

import Layout from "../components/shared/Layout";

import styles from "../styles/pages/read.module.scss";

import profilePic from '../public/img/read_image.jpg'

const Read = () => {
    return (
        <Layout>
            <div className={styles.read}>
                <div className={styles.read_column_left}>
                    <div className={styles.read_img}>
                        <Image src={profilePic} alt="Picture of the bear"/>
                    </div>
                </div>
                <div className={styles.read_column_right}>
                    <h3 className={styles.read_text_heading}>2085. ПИВО З МАЙБУТНЬОГО</h3>
                    <p className={styles.read_text}>2085 це пиво з майбутнього. Ідеться не лише про 2085 рік — ми
                        говоримо про наш футуристичний погляд на світ
                        у цілому й пиво зокрема.
                        Навіть 2085 року ми варитимемо пиво з майбутнього.
                    </p>
                    <h3 className={styles.read_text_heading}>КЛАСИКА 2085</h3>
                    <p className={styles.read_text}>Крафт 2085 — сміливе й смачне пиво.</p>
                    <p className={styles.read_text}>Смачне, бо зварив його кращий бровар. Сміливе, бо не боїться
                        експериментів.</p>
                    <p className={styles.read_text}>Ми маємо власний завод, досвід і жваву фантазію.
                        Ми варимо класичні та експериментальні сорти. 2085 року наші експерименти стануть класикою.
                    </p>
                    <h3 className={styles.read_text_heading}>ОБЛАДНАННЯ</h3>
                    <p className={styles.read_text}>Ми першими в Україні почали варити крафт на обладнанні American
                        Beer Equipment.</p>
                    <h3 className={styles.read_text_heading}>ПРОСТІР</h3>
                    <p className={styles.read_text}>Пивзавод 2085 розміщено на території артзаводу «Платформа». Тут
                        же найсвіжіша варка.&nbsp;</p>
                    <h3 className={styles.read_text_heading}>КОМАНДА</h3>
                    <p className={styles.read_text}>Назар Дребот. Душа проекту. 7 років йшов до створення пивоварні
                        масштабу 2085.
                        За цей час отримав 6 міжнародних пивних нагород і запустив
                        7 крафтових пивоварень (в США, Грузії, Іспанії, Індії та Україні).
                    </p>
                    <p className={styles.read_text}>Валерій Созановський. Ідейний натхненник і рушій проєкту.
                        Ресторатор, підприємець, співзасновник сім’ї ресторанів THE.
                    </p>
                    <p className={styles.read_text}>Володимир Володарський. Бізнесмен, співзасновник сім’ї
                        ресторанів THE.</p>
                    <p className={styles.read_text}>Fedoriv agency. Дизайн продукту, комунікація.</p>
                    <p className={styles.read_text}>Balbek Bureau. Дизайн інтер'єру.</p>
                    <p>
                        <br/>
                    </p>
                    <p className={styles.read_text}>Ми не знаємо, яким буде світ 2085 року, але знаємо, яке пиво
                        питимуть 2085 року.</p>
                    <p>
                        <br/>
                    </p>
                    <p className={styles.read_text}><strong>2085 pack / Order now</strong><br/>&nbsp;</p>
                </div>
            </div>
        </Layout>
    );
};

export default Read;
