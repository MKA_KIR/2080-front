import Layout from "../components/shared/Layout";
import MainHome from "../components/MainHome";

export default function Home() {
    return (
        <Layout>
            <MainHome/>
        </Layout>
    )
}
